<!-- PROJECT LOGO -->
<p align="center">
  <h3 align="center">GL Transmission Format Converter</h3>

  <p align="center">
    Great FBX, OBJ to glTF Converter
    <br />
    <a href="https://cloud.sc-lab.kr:50000/sws5779/gl_transmission_format_converter"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://cloud.sc-lab.kr:50000/sws5779/gl_transmission_format_converter">View Demo</a>
    ·
    <a href="https://cloud.sc-lab.kr:50000/sws5779/gl_transmission_format_converter">Report Bug</a>
    ·
    <a href="https://cloud.sc-lab.kr:50000/sws5779/gl_transmission_format_converter">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#usage">Video</a></li>
    <li><a href="#roadmap">License</a></li>
    <li><a href="#contributing">Contact</a></li>
    <li><a href="#license">Acknowledgement</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
3D 데이터 포맷 중 glTF는 여러 방법으로 획득할 수 있음. 하지만, glTF 중 일부 구성 요소(카메라 및 light)가 개발 환경에 악영향을 끼쳐 문제를 야기할 가능성이 있음. 해당 문제는 프로그램 개발 기간이 늘어나는 결과를 초래함. 특히 웹 환경에서는 디버깅이 어렵기 때문에 모델은 간단할 필요성이 있음. 해당 프로젝트는 다양한 3D 데이터 포맷을 웹 환경에서 간단한 glTF 포맷으로 변환함

Here's why:
* 웹 환경에서는 glTF 포맷이 최적화되어 있음
* 웹 환경과 모델 환경이 서로 영향을 줄 가능성이 있음
* glTF 포맷은 가장 압축률이 높음

Of course, no one template will serve all projects since your needs may be different. So I'll be adding more in the near future. You may also suggest changes by forking this repo and creating a pull request or opening an issue.

A list of commonly used resources that I find helpful are listed in the acknowledgements.

### Built With

본 프로젝트에서는 다음과 같은 모듈을 사용했습니다.
* [Runtime OBJ Importer](https://assetstore.unity.com/packages/tools/modeling/runtime-obj-importer-49547)
* [UniGLTF](https://github.com/ousttrue/UniGLTF)


# Video
![obj](/uploads/330d7cb0c7bba361ddf011150f6e5aac/obj.gif)

![fbx](/uploads/7ab9835d5387a4873db02bfd4bb72e4d/fbx.gif)

<!-- LICENSE -->
# License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
# Contact

[Soonchul Kwon] - ksc0226@kw.ac.kr

Link: [Spatial Computing Lab.](http://sc-lab.kr/)



<!-- ACKNOWLEDGEMENTS -->
# Acknowledgement
* This work was supported by Institute of Information & Communications Technology Planning & Evaluation (IITP) grant funded by the Korean government (MSIT) (No.2020-0-00192, AR Cloud, Anchor, Augmented Reality, Fog Computing, Mixed Reality).
